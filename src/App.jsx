import React from "react";
import { BrowserRouter, Link, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importamos los componentes de la aplicación (vistas)
import Inicio from './Inicio';
import Lista from './Lista';
import NuevoContacto from './NuevoContacto';
import ModificaContacto from './ModificaContacto';
import EliminaContacto from './EliminaContacto';
import P404 from './P404';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './app.css';

// importamos modelo
import Contacto from './Contacto';

// clase App 
export default class App extends React.Component {

  constructor(props) {
    super(props);

    const contactosInicio = [
      new Contacto(1, "indiana", "indiana@jones.com"),
      new Contacto(2, "007", "james@bond.com"),
      new Contacto(3, "spiderman", "peter@parker.com"),
    ];

    this.state = {
      contactos: contactosInicio,
      ultimoContacto: 3
    };

    this.guardaContacto = this.guardaContacto.bind(this);
    this.eliminaContacto = this.eliminaContacto.bind(this);
    this.saveData = this.saveData.bind(this);
    this.loadData = this.loadData.bind(this);
    

  }



//extra guardado de datos
saveData(){
  var jsonData = JSON.stringify(this.state);
  localStorage.setItem("datagenda", jsonData);
}
         
//carga de datos
loadData(){
  var text = localStorage.getItem("datagenda");
  if (text){
      var obj = JSON.parse(text);
      this.setState(obj);
  }
}
  
  guardaContacto(datos) {
    //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
    if (datos.id===0){
      datos.id = this.state.ultimoContacto + 1;
      this.setState({ultimoContacto: datos.id});
    }
    // comprobaciones adicionales... email ya existe? datos llenos?
    // si todo ok creamos contacto y lo añadimos a la lista
    let nuevo = new Contacto(datos.id, datos.nombre, datos.email);
    // si contacto existe lo eliminamos!
    // esto es porque podemos llegar aquí desde nuevo contacto o desde modifica contacto
    let nuevaLista = this.state.contactos.filter( el => el.id!==nuevo.id);
    //añadimos elemento recien creado
    nuevaLista.push(nuevo);
    // finalmente actualizamos state
    this.setState({contactos: nuevaLista});
    
  }


  eliminaContacto(idEliminar) {
    //creamos lista a partir de state.contactos, sin el contacto con el id recibido
    let nuevaLista = this.state.contactos.filter( el => el.id!==idEliminar);
    //asignamos a contactos
    this.setState({contactos: nuevaLista});
    
  }

  render() {

    if (this.state.contactos.length === 0){
      return <h1>Cargando datos...</h1>;
    }
    

    return (
      <BrowserRouter>
        <Container>
          <Row>
            <Col>
              <ul className="list-unstyled menu">
                <li> <NavLink className="link" to="/">Inicio</NavLink> </li>
                <li> <NavLink className="link" to="/lista">Contactos</NavLink> </li>
                <li> <NavLink className="link" to="/nuevo">Añadir</NavLink> </li>
              </ul>
              
            </Col>
            </Row>
            <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inicio} />
                <Route path="/lista" render={()=><Lista contactos={this.state.contactos} />} />
                <Route path="/nuevo" render={() => <NuevoContacto guardaContacto={this.guardaContacto} />} />
                <Route path="/modifica/:idContacto" render={(props) => <ModificaContacto {...props} contactos={this.state.contactos} guardaContacto={this.guardaContacto} />} />
                <Route path="/elimina/:idContacto" render={(props) => <EliminaContacto {...props} contactos={this.state.contactos} eliminaContacto={this.eliminaContacto} />}  />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>

          <Row>
            <Button onClick={this.loadData}>Cargar datos</Button>
            <Button onClick={this.saveData}>Guardar datos</Button>
          </Row>

        </Container>
      </BrowserRouter>
    );

  }

}
