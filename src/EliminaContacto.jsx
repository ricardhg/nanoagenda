
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button} from 'reactstrap';


class EliminaContacto extends Component {
    constructor(props) {
        super(props);

        //leemos datos recibidos

        let id = this.props.match.params.idContacto * 1;
        let listaDeUno = this.props.contactos.filter(el => el.id===id);
        let actual = listaDeUno[0];

        this.state = {
            nombre: actual.nombre,
            email: actual.email,
            id: actual.id,
            volver: false
        };

        this.eliminar = this.eliminar.bind(this);
        this.noeliminar = this.noeliminar.bind(this);

    }

    eliminar(){
        this.props.eliminaContacto(this.state.id);
        this.setState({volver: true});
    }


    noeliminar(){
        this.setState({volver: true});
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to='/lista' />
        }

        return (
            <>
                <h3>Desea elminar a {this.state.nombre}</h3>
                <Button onClick={this.eliminar} color="danger">Sí</Button>
                <Button onClick={this.noeliminar} color="success">No</Button>
            </>

        );
    }
}






export default EliminaContacto;
