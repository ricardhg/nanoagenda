import React from "react";

//https://fontawesome.com/v4.7.0/icons/

export default () => (
    <>
        <i className="fa fa-4x fa-users"></i>
        <h2>Nano Agenda</h2>
    </>
);