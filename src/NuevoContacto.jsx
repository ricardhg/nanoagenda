
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';


class NuevoContacto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            errorNombre: false,
            email: '',
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
        this.cancelar = this.cancelar.bind(this);
    }

    //gestión genérica de cambio en campo input
    cambioInput(event) {
        const v =  event.target.value;
        const n = event.target.name;
        this.setState({
            [n]: v
        });
    }

    cancelar(){
        this.setState({ volver: true });
    }

    //método activado al enviar el form (submit)
    submit(e) {
        e.preventDefault();
        //validación
        if(this.state.nombre===''){
            this.setState({ errorNombre: true });
            return;
        }
        //ejecutar si validación ok
        this.props.guardaContacto({
            nombre: this.state.nombre,
            email: this.state.email,
            id: 0
        });
        this.setState({ volver: true });
    }

    render() {
        //si se activa volver redirigimos a lista
        if (this.state.volver === true) {
            return <Redirect to='/lista' />
        }

        let claseErrorNombre = "mensajeError";
        if (this.state.errorNombre===true){
            claseErrorNombre = "mensajeError visible";
        }

        return (

            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nombreInput">Nombre</Label>
                            <Input type="text" 
                                name="nombre" 
                                id="nombreInput"
                                value={this.state.nombre}
                                onChange={this.cambioInput} />
                                <p className={claseErrorNombre}>Campo obligatorio</p>
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">E-mail</Label>
                            <Input type="text" name="email" id="emailInput"
                                value={this.state.email}
                                onChange={this.cambioInput} />
                        </FormGroup>
                    </Col>
                </Row>


                <Row>
                    <Col>
                        <Button type="submit" color="primary">Guardar</Button>
                    </Col>
                    <Col>
                        <Button type="button" onClick={this.cancelar} color="danger">Cancelar</Button>
                    </Col>
                </Row>
            </Form>

        );
    }
}






export default NuevoContacto;
